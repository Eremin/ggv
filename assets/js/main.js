// Используемы в программе идентификаторы
var ids = {
        canvas: "graph-container__wall__graph",
        file: "data__left-column__select-file__file",
        limits_button: "limits_button",
        history_data: "history_data",
        fraction: "fraction",
        min_limits_checkbox: "min_limits",
        x_min_input: "x_min",
        y_min_input: "y_min",
        scaling_value_input: "graph-container__scaling-buttons__value",
        scaling_up_btn: "graph-container__scaling-buttons__up",
        scaling_down_btn: "graph-container__scaling-buttons__down"
    },

    // Шаги подготовки для выполнения программы
    steps = {
        1: [false, document.getElementById("step-1")],
        2: [false, document.getElementById("step-2")],
        3: [false, document.getElementById("step-3")],
        4: [false, document.getElementById("step-4")],
        5: [false, document.getElementById("step-5")]
    }

    // canvas с графиком
    plot = document.getElementById(ids.canvas),
    ctx = plot.getContext("2d"),

    // Input выбора файла
    plot_file = document.getElementById(ids.file),
    background = new Image(),

    current_x = document.getElementsByClassName("current_x"),
    current_y = document.getElementsByClassName("current_y"),

    x0 = document.getElementsByClassName("x0")
    y0 = document.getElementsByClassName("y0"),
    x1 = document.getElementsByClassName("x1"),
    y1 = document.getElementsByClassName("y1"),
    x_min_EL = document.getElementById(ids.x_min_input),
    y_min_EL = document.getElementById(ids.y_min_input),
    x_max_EL = document.getElementById("x_max"),
    y_max_EL = document.getElementById("y_max"),
    x_value_EL = document.getElementsByClassName("x_value"),
    y_value_EL = document.getElementsByClassName("y_value"),
    history_data_EL = document.getElementById(ids.history_data),
    fraction_EL = document.getElementById(ids.fraction),
    min_limits_checkbox = document.getElementById(ids.min_limits_checkbox),
    scaling_value_input = document.getElementById(ids.scaling_value_input),
    scaling_up_btn = document.getElementById(ids.scaling_up_btn),
    scaling_down_btn = document.getElementById(ids.scaling_down_btn),

    x0_position = 0,
    y0_position = 0,
    x1_position = 0,
    y1_position = 0,
    click_number = 0,

    // Пределы значений графика
    is_min_limits = false, // default нижние пределы отключены
    x_min = 0,
    y_min = 0,

    x_max = 0,
    y_max = 0,

    scaling = 1,

    // Можно ли вычислять значения в размерностях графика
    can_culc_values = false;

    // Значения в размерностях графика
    x_value = 0,
    y_value = 0;

    // Ждем когда пользователь выберет график
    plot_file.addEventListener('change', function(){
        // Если шаг 1 уже выполнен - ничего не делаем
        if (steps[1][0]) {
            return;
        }

        // Формируем path графика
        background.src = "./graphs/" + this.files[0].name;
        // Когда график загрузился
        background.onload = function(){
            // Устанавливаем размеры холста и отображаем график
            plot.width = background.naturalWidth;
            plot.height = background.naturalHeight;
            plot.style.display = "block";
            ctx.drawImage(background, 0, 0);

            // Запрещаем выбирать новые графики
            plot_file.disabled = true;
            // и меняем состояние шага 1
            steps[1][0] = true;
            steps[1][1].style.backgroundColor = "rgb(240, 255, 240)";

            // Разрешаем его масштабировать
            scaling_down_btn.disabled = false;
            scaling_up_btn.disabled = false;
            scaling_value_input.disabled = false;
        } 
    });

    function scalingUp(){
        var event = new Event('input');

        scaling = Math.round((scaling + 0.1) * 100) / 100;
        scaling_value_input.value = Math.round(scaling * 100);

        scaling_value_input.dispatchEvent(event);
    };

    function scalingDown(){
        var event = new Event('input');

        scaling = Math.round((scaling - 0.1) * 100) / 100;
        scaling_value_input.value = Math.round(scaling * 100);

        scaling_value_input.dispatchEvent(event);

    };

    // Отслеживаем изменение масштаба графика
    scaling_value_input.addEventListener('input', function(){
        var w = 0,
            h = 0;

        // Получаем новое значение scaling
        scaling = Math.round((parseInt(this.value) / 100) * 100) / 100,

        w = background.naturalWidth * scaling;
        h = background.naturalHeight * scaling;

        // Увеличиваем рзмеры графика
        plot.width = w;
        plot.height = h;
        ctx.drawImage(background, 0, 0, w, h);
    })

    // Отображение координат при перемещении курсора внутри canvas'a
    plot.addEventListener("mousemove", function (e) {
        var mouse_pos = getMousePos(this, e),
            mouse_values = {};
        
            setInnerHTMLForSomeEls(current_x, "(" + mouse_pos.x);
            setInnerHTMLForSomeEls(current_y, mouse_pos.y + ")");
        
        if (can_culc_values) {
            mouse_values = getMouseValues(mouse_pos.x, mouse_pos.y);
            setInnerHTMLForSomeEls(x_value_EL, "(" + mouse_values.x);
            setInnerHTMLForSomeEls(y_value_EL, mouse_values.y + ")");
        }
    }, false);

    // Ждем когда изменится значение checkbox'а
    min_limits_checkbox.addEventListener('change', function(){
        if (this.checked) {
            is_min_limits = true;
            x_min_EL.style.display = "inline";
            y_min_EL.style.display = "inline";
        } else {
            is_min_limits = false;
            x_min_EL.style.display = "none";
            y_min_EL.style.display = "none";
        }
    })

// Проверка шага 2
function check2Step(){
    var limits_button = document.getElementById(ids.limits_button);

    if (is_min_limits) {
        x_min = x_min_EL.value;
        y_min = y_min_EL.value;
    }

    x_max = x_max_EL.value;
    y_max = y_max_EL.value;

    // Запрещаем указывать новые пределы
    min_limits_checkbox.disabled = true;
    x_min_EL.disabled = true;
    y_min_EL.disabled = true;
    x_max_EL.disabled = true;
    y_max_EL.disabled = true;
    limits_button.disabled = true;
    
    // Меняем состояние шага 2
    steps[2][0] = true;
    steps[2][1].style.backgroundColor = "rgb(240, 255, 240)";
}

plot.addEventListener("click", function (e) {
    var mouse_pos = getMousePos(this, e),
        mouse_values = getMouseValues(mouse_pos.x, mouse_pos.y),
        history_data = history_data_EL.innerHTML;
    
    // Если не выполнен шаг 2 - ничего не делаем
    if (!steps[2][0]) {
        return;
    }

    click_number += 1;
    // Устанавливаем точку начала координат
    if (click_number == 1) {
        x0_position = mouse_pos.x;
        y0_position = mouse_pos.y;
        setInnerHTMLForSomeEls(x0, "(" + x0_position);
        setInnerHTMLForSomeEls(y0, y0_position + ")");
        // и меняем состояние шага 3
        steps[3][0] = true;
        steps[3][1].style.backgroundColor = "rgb(240, 255, 240)";

        // Запрещаем менять масштаб графика
        scaling_down_btn.disabled = true;
        scaling_up_btn.disabled = true;
        scaling_value_input.disabled = true;
    }
    // Определяем положение оси X
    else if (click_number == 2) {
        x1_position = mouse_pos.x;
        setInnerHTMLForSomeEls(x1, "(" + x1_position);
        // и меняем состояние шага 4
        steps[4][0] = true;
        steps[4][1].style.backgroundColor = "rgb(240, 255, 240)";
    }
    // Определяем положение оси Y
    else if (click_number == 3) {
        y1_position = mouse_pos.y;
        setInnerHTMLForSomeEls(y1, y1_position + ")");
        can_culc_values = true;
        // и меняем состояние шага 4
        steps[5][0] = true;
        steps[5][1].style.backgroundColor = "rgb(240, 255, 240)";
    }
    else {
        var fill_rect_size = Math.floor(3 * scaling),
            half_fill_rect_filze = 0;

        if ((fill_rect_size % 2) == 0) {
            fill_rect_size = fill_rect_size - 1;
        }

        half_fill_rect_filze = Math.floor(fill_rect_size / 2);

        history_data_EL.innerHTML = history_data + mouse_values.x + " ; " + mouse_values.y + "<br>";
        ctx.fillRect(mouse_pos.x - 50 - half_fill_rect_filze, mouse_pos.y - 50 - half_fill_rect_filze, fill_rect_size, fill_rect_size);
    }


}, false);


    function getMousePos(canvas, e) {
        var rect = canvas.getBoundingClientRect();
        return {
          x: Math.round(e.clientX - rect.left),
          y: Math.round(e.clientY - rect.top)
        };
    }

    function getMouseValues(x, y) {
        // Разбираемся с осью X
        var x = x - x0_position,
            y = y0_position - y,
            Dx = x1_position - x0_position,
            Dy = y0_position - y1_position,
            dx_value = (x_max - x_min) / Dx,
            dy_value = (y_max - y_min) / Dy,
            x_value = parseFloat(x_min) + x * dx_value,
            y_value = parseFloat(y_min) + y * dy_value,
            fraction = fraction_EL.value;

        if (!fraction) {
            fraction = 0;
        }
        fraction = Math.pow(10, fraction)
        
        return {
            x: Math.round(x_value * fraction) / fraction,
            y: Math.round(y_value * fraction) / fraction
        };
    }

    function setInnerHTMLForSomeEls(els, innerHTML) {
        for (el in els) {
            els[el].innerHTML = innerHTML;
        }
    }